teamloop v2.0
======================

All actions should be presented with "Request" and injected with whatever services they require to do their job

All actions should yield with a "Response"

All actions have 1 required method to implement:

def _execute
  // implement the actual execution of this action
  // should be focused on the smallest task possible that makes sense, favor multiple actions for complex tasks

  // this method should set @response_verified = true once the response has been executed and verified. This does not
  // mean the response is successful it just means that the response should be considered safe and that any security
  // checks that needed to happen have been done.
end

Along with 1 required method, there is 1 optional method that can be overriden:

def _verify
  // this method is called before _execute and is used to verify that the request contains all required information and is not
  // malformed in any way. While it should be used to make sure the action has all the data it needs to execute it should not
  // be responsible for validating that data in any way (especially in any way that requires connecting to external services).
  // A default implementation that checks that identity is provided will be ran by default if this method is not implemented.

  // this method must set @request_verified = true once the request has been verified not to be malformed and to have all required
  // information.
end

Favor data structures over entities
**Entities (i.e. Classes) are for encapsulating functionality so rule of thumb is:
** 1) If piece of data needs more than 1 piece of encapsulating functionality (excluding validations)
      then an entity MAY be created.



Architectural Notes
--------------------
./lib => core application files
./delivery => delivery mechanism (web application)


Teamloop Configuration
----------------------
All configuration will be done via: TeamloopConfig

Configuring gateways, background queue, etc, that is required by interactors will be setup using TeamloopConfig (i.e.):

TeamloopConfig.configure do
  OrganizationGateway MyOrgGatewayImpl
  TeamGateway MyTeamGatewayImpl
  Queue MyBackgroundQueueImpl
end


Run All tests
----------------------
From root, run:

> rake test

For Verbose mode:

> rake test TESTOPTS="-v"


Testing Notes
----------------------

1 test = 1 assertion, if you want another assertion do another test

Opt for "refute_equal" when testing things that are expected to be wrong (in place of assert_equal of a falsy value)

Opt for "assert_equal" when testing things that are exptected to be right