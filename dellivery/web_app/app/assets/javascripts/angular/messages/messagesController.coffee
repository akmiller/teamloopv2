#Messages Controller

@MessagesCtrl = MessagesCtrl = ($scope, $route, $routeParams, $location) ->
  $scope.name = "MessagesCtrl"
  $scope.params = $routeParams

  $scope.messages = [
    { 'id': 1, 'message': 'test message'},
    { 'id': 2, 'message': 'test 2 message'}
  ]