
# Teams Controller

@TeamsCtrl = TeamsCtrl = ($scope, $route, $routeParams, $location, Team) ->
  $scope.name = "TeamsCtrl"
  $scope.params = $routeParams
  $scope.teams = Team.query()

  $scope.save = (team_data) ->
    team = new Team(team_data)
    team.$save(
      (team, headers) ->
        result_headers = headers() 
        $location.path result_headers["location"] 
      (err) ->
        console.log err
        alert "Error: " + err.status
      )


