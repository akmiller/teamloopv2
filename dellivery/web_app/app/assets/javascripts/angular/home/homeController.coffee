
# teamloop controller

@HomeCtrl = HomeCtrl = ($scope, $route, $routeParams, $location) ->
  $scope.$route = $route
  $scope.$location = $location
  $scope.$routeParams = $routeParams

  $scope.activities = [
    { 'when': 'today at 4:13pm', 'description' : 'Adam posted a new message for the Mavericks' },
    { 'when': 'today at 3:34pm', 'description' : 'Adam added a new photo for the Mavericks.' }
  ]

  $scope.events = [
    { 'when': 'Today - 7:00pm (1 hour)', 'location': { title: 'Hawthorne Elementary'}, 'details': 'Please arrive 10 minutes early for stretches' },
    { 'when': 'Dec. 5th - 7:00pm (1 hour)', 'location': { title: 'Hawthorne Elementary'}, 'details': '' }
  ]
    
