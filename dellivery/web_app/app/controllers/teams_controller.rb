class TeamsController < ApplicationController

  respond_to :json

  def index
    req = Request.new(_get_identity, { :team => { :id => 5, :name => "DirtDawgs", :activity => "Baseball", :time_zone => "CST" }})

    get_all_teams = Teams::GetAllForUser.new(req).run do |resp|
      respond_to do |format|

        if resp.success?
          format.json { render :json => resp.data[:teams] }
        else
          respond_to_failure resp, format
        end

      end
    end
  end

  def create
    req = Request.new(_get_identity, { :team => params[:team] })

    create_team_action_queue = ActionQueue.new(req,
      [
        Teams::CanCreate,
        Teams::IsValid,
        Teams::Create,
      ],
      deps = { :team_gateway => InMemoryEntityGateway.new }
    )

    create_team_action_queue.run do |resp|
      respond_to do |format|
        if resp.success?
          format.json { render :json => resp.data, :status => :created, :location => "/teams" }
        else
          respond_to_failure resp, format
        end
      end
    end
  end

end
