class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :intercept_html_requests
  layout nil

  def _get_identity
    @identity = Identity.create_test_identity 
  end

  def is_authenticated?
    true 
  end

  def intercept_html_requests
    if request.format == Mime::HTML
        render "layouts/dynamic", :layout => false if is_authenticated?
    end
  end

  def respond_to_failure(response, format)
    errors = Array.new
    errors << response.errors unless response.errors.nil?

    json_error_info = {
      :success => false,
      :errors => errors
    }

    format.json { render :json => json_error_info, :status => :unprocessable_entity } 
  end

end
