require 'minitest/autorun'

require './lib/teamloop'

class TestTeamActions < MiniTest::Unit::TestCase
  def setup
    #@identity = Identity.new(33, "akmiller@gmail.com")
    @identity = Struct.new(:id, :email)

    TeamloopConfig.configure do
      EntityGateway InMemoryEntityGateway.new({ :reset_data => true }) 
    end
  end

  def test_can_create_new_team_with_missing_identity
    can_create = Teams::CanCreate.new(Request.new(nil))

    can_create.run do |resp|
      assert_equal false, resp.success?
    end
  end

  def test_can_create_new_team_with_valid_identity
    can_create = Teams::CanCreate.new(Request.new(@identity))

    can_create.run do |resp|
      assert_equal true, resp.success?
    end
  end

=begin
## TEST: validate_team_data  
=end  
  def test_validate_team_data_with_missing_identity
    is_valid = Teams::IsValid.new(Request.new(nil))

    is_valid.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_refute_validate_team_data_with_missing_name
    is_valid = Teams::IsValid.new(Request.new(@identity, { :team => { :activity => "Baseball", :time_zone => "CST" }}))

    is_valid.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_refute_validate_team_data_with_missing_activity
    is_valid = Teams::IsValid.new(Request.new(@identity, { :team => { :name => "DirtDawgs", :time_zone => "CST" }}))

    is_valid.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_refute_validate_team_data_with_missing_timezone
    is_valid = Teams::IsValid.new(Request.new(@identity, { :team => { :name => "DirtDawgs", :activity => "Baseball" }})) 

    is_valid.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_validate_team_data_with_all_valid_fields
    is_valid = Teams::IsValid.new(Request.new(@identity, { :team => { :name => "DirtDawgs", :activity => "Baseball", :time_zone => "CST" }}))

    is_valid.run do |resp|
      assert_equal true, resp.success?
    end
  end

  def test_get_all_teams_with_missing_identity

    req = Request.new(nil)
    get_all_teams = Teams::GetAllForUser.new(req)

    get_all_teams.run do |resp|
      assert_equal resp.errors.count, 1
    end

  end

  def test_isvalid_and_cancreate_actionqueue

    req = Request.new(@identity, { :team => { :id => 5, :name => "DirtDawgs", :activity => "Baseball", :time_zone => "CST" }})
    
    aq = ActionQueue.new(req, [Teams::IsValid, Teams::CanCreate, Teams::Create], deps = { :team_gateway => InMemoryEntityGateway.new})

    aq.run do |resp|
      assert_equal true, resp.success?
    end    

  end

end