require 'minitest/autorun'

require './lib/teamloop'

class TestSeasonTeamActions < MiniTest::Unit::TestCase
  def setup
    #@identity = Identity.new(33, "akmiller@gmail.com")
    @identity = Struct.new(:id, :email)

    TeamloopConfig.configure do
      EntityGateway InMemoryEntityGateway.new({}) 
    end

    req = Request.new(@identity, { :team => { :id => 5, :name => "DirtDawgs", :activity => "Baseball", :time_zone => "CST" }})
    #res = TeamInteractor::create(req)

    #@default_team = res.data[:team]
  end
=begin
  def test_season_with_invalid_dates
    req = Request.new(@identity, 
      { 
        :team => @default_team, 
        :season => {
          :title => "Fall 2012",
          :starts_on => Time.new,
          :ends_on => Time.new("2010-01-01") 
        }
      })

    resp = SeasonInteractor::validate(req)

    refute_equal true, resp.success?
  end

  def test_create_season_with_missing_title
    req = Request.new(@identity, 
      { 
        :team => @default_team, 
        :season => {
          :starts_on => Time.new,
          :ends_on => Time.new("2050-01-01") 
        }
      })

    resp = SeasonInteractor::validate(req)

    refute_equal true, resp.success?
  end

  def test_create_season_with_empty_title
    req = Request.new(@identity, 
      { 
        :team => @default_team, 
        :season => {
          :title => "",
          :starts_on => Time.new,
          :ends_on => Time.new("2050-01-01") 
        }
      })

    resp = SeasonInteractor::validate(req)

    refute_equal true, resp.success?
  end

  def test_create_season_with_valid_defaults
    req = Request.new(@identity, 
      { 
        :team => @default_team, 
        :season => {
          :title => "Fall 2012",
          :starts_on => Time.new,
          :ends_on => Time.new("2050-01-01") 
        }
      })

    resp = SeasonInteractor::validate(req)

    assert_equal true, resp.success?
  end
=end
end
