require 'minitest/autorun'

require './lib/teamloop'

class TestUserActions < MiniTest::Unit::TestCase
  def setup
    #@identity = Identity.new(33, "akmiller@gmail.com")
    @identity = Struct.new(:id, :email)

    @deps = {
      :user_gateway => InMemoryEntityGateway.new({ :reset_data => true }),
      :password_service => TeamloopServices::PlainTextPasswordService
    }

    TeamloopConfig.configure do
      EntityGateway InMemoryEntityGateway.new({ :reset_data => true }) 
    end
  end

  def test_user_has_valid_credentials
    valid_credentials = Users::HasValidCredentials.new(Request.new(nil, { :user => { :email => "akmiller@gmail.com", :password => "passord1" } }))

    valid_credentials.run do |resp|
      assert_equal true, resp.success?
    end
  end

  def test_user_missing_email_credential
    valid_credentials = Users::HasValidCredentials.new(Request.new(nil, { :user => { :password => "pass" } }))

    valid_credentials.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_user_missing_password_credential
    valid_credentials = Users::HasValidCredentials.new(Request.new(nil, { :user => { :email => "akmiller@gmail.com" } }))

    valid_credentials.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_user_missing_request_data
    valid_credentials = Users::HasValidCredentials.new(Request.new(nil)) 

    valid_credentials.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_authenticate_with_invalid_user
    auth_action = Users::AuthenticateWithPassword.new(Request.new(nil, { :credentials => { :email => "akmiller+100@gmail.com", :password => "teamloopv2" }}), @deps)

    auth_action.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_authenticate_with_invalid_password
    user_gateway_mock = MiniTest::Mock.new

    user_gateway_mock.expect(:find_user_by_email, { :email => "akmiller@gmail.com", :password => "teamloopv2" }, ['akmiller@gmail.com'])
    password_service = TeamloopServices::PlainTextPasswordService

    deps = {
      :user_gateway => user_gateway_mock,
      :password_service => password_service
    }

    auth_action = Users::AuthenticateWithPassword.new(Request.new(nil, { :credentials => { :email => "akmiller@gmail.com", :password => "wrong--teamloopv2" }}), deps)
    auth_action.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_authenticate_with_unknown_email
    user_gateway_mock = MiniTest::Mock.new

    user_gateway_mock.expect(:find_user_by_email, nil, ['akmiller@gmail.com'])
    password_service = TeamloopServices::PlainTextPasswordService

    deps = {
      :user_gateway => user_gateway_mock,
      :password_service => password_service
    }

    auth_action = Users::AuthenticateWithPassword.new(Request.new(nil, { :credentials => { :email => "akmiller@gmail.com", :password => "teamloopv2" }}), deps)
    auth_action.run do |resp|
      refute_equal true, resp.success?
    end
  end

  def test_authenticate_with_valid_user
    user_gateway_mock = MiniTest::Mock.new

    user_gateway_mock.expect(:find_user_by_email, { :email => "akmiller@gmail.com", :password => "teamloopv2" }, ['akmiller@gmail.com'])
    password_service = TeamloopServices::PlainTextPasswordService

    deps = {
      :user_gateway => user_gateway_mock,
      :password_service => password_service
    }

    auth_action = Users::AuthenticateWithPassword.new(Request.new(nil, { :credentials => { :email => "akmiller@gmail.com", :password => "teamloopv2" }}), deps)
    auth_action.run do |resp|
      assert_equal true, resp.success?
    end
  end

  def test_create_user_success
    user_gateway_mock = MiniTest::Mock.new

    user_gateway_mock.expect(:save_user, { :user => { :email => "akmiller@gmail.com", :password => "teamloopv2" } }, 
      [{
        :user => {
          :email => "akmiller@gmail.com",
          :password => "teamloopv2"
        }
      }])
    password_service = TeamloopServices::PlainTextPasswordService

    deps = {
      :user_gateway => user_gateway_mock,
      :password_service => password_service
    } 

    create = Users::Create.new(Request.new(nil, { :user => { :email => "akmiller@gmail.com", :password => "teamloopv2" }}), deps)
    create.run do |resp|
      assert_equal true, resp.success?
    end
  end

  def test_create_user_success_returns_same_email
    user_gateway_mock = MiniTest::Mock.new

    user_gateway_mock.expect(:save_user, { :user => { :email => "akmiller@gmail.com", :password => "teamloopv2" } }, 
      [{
        :user => {
          :email => "akmiller@gmail.com",
          :password => "teamloopv2"
        }
      }])
    password_service = TeamloopServices::PlainTextPasswordService

    deps = {
      :user_gateway => user_gateway_mock,
      :password_service => password_service
    } 

    create = Users::Create.new(Request.new(nil, { :user => { :email => "akmiller@gmail.com", :password => "teamloopv2" }}), deps)
    create.run do |resp|
      assert_equal "akmiller@gmail.com", resp.data[:user][:email]
    end
  end

end