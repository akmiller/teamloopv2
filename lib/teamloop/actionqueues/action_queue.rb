class ActionQueue
  attr_reader :request, :action_queue, :dependencies

  def initialize(request, queue = [], deps = {})
    #populate internal queue with correct order (by reversing what the array was)
    # this assumes user populates array as [step 1, step 2, step 3]
    @request = request
    @action_queue = queue.reverse
    @dependencies = deps
  end

  def push(action)
    @action_queue.push action
  end

  def pop(action)
    @action_queue.pop action
  end

  #run all actions in queue as long as resp.success? returns true
  def run
    response = nil

    action = @action_queue.pop
    while !action.nil?
      action_to_run = action.new(@request, @dependencies)

      action_to_run.run do |resp|
        response = resp
        if !resp.success?
          break
        end
      end

      action = @action_queue.pop
    end

    yield response

  end

end