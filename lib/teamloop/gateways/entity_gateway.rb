class EntityGateway
  attr :options

  def initialize(options = {})
    @options = options
  end

  def find_user_by_email(email)
    raise "#{__method__} NOT IMPLEMENTED IN GATEWAY"
  end

  def save_user(user_data)
    raise "#{__method__} NOT IMPLEMENTED IN GATEWAY"
  end

  def find_teams(identity)
    raise "#{__method__} NOT IMPLEMENTED IN GATEWAY"
  end

  # retrieve just basic team information
  def get_team_by_id(id)
    raise "#{__method__} NOT IMPLEMENTED IN GATEWAY"
  end

  # retrieve a team and all (or most) of it's related data (upcoming events, recent messages, roster, etc...)
  def get_team_by_id_with_related_data(id)
    raise "#{__method__} NOT IMPLEMENTED IN GATEWAY"
  end

  def save_team(identity, team_data)
    raise "#{__method__} NOT IMPLEMENTED IN GATEWAY"
  end

end