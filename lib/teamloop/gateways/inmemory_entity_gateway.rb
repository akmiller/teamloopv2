#NOT TO BE USED FOR PRODCUTION, ONLY TESTING AND DEMONSTRATIONS OF FUNCTIONALITY
require 'teamloop/gateways/entity_gateway'

class InMemoryEntityGateway < EntityGateway

  @@storage = nil 

  def initialize(options = {})
    super(options)

    # if internal storage is not initialized or we are reqeusting reset (reset used in unit tests)
    if @@storage.nil? || (options.has_key?(:reset_data) && options[:reset_data]) 
      @@storage = Hash.new
    end

  end

  def list_keys
    #@@in_memory_team_hash.each_key { |key| puts key }

    @@storage.each_key { |key| puts key }
  end

  def validate_identity(identity)
    if identity.type == :forms
      true
    else
      false
    end
  end

  def find_user_by_email(email)
    result = nil

    data = @@storage[:users]
    if !data.nil?
      data.each do |item|
        if item[:email] == email
          result = item
          break
        end
      end
    end

    result
  end

  def save_user(user_data)
    if user_data.has_key?(:id)
      _update(:users, user_data)
    else
      _insert(:users, user_data)
    end
  end

  def find_teams(identity)
    #just return all teams for now
    teams = @@storage[:teams]
    if teams.nil?
      teams = Array.new
    end

    teams
  end
 
  def get_team_by_id(id)
    _find_by_id(:teams, id)
  end

  def save_team(data)
    # data[:id] should be key, if not present, then assume new record
    if data.has_key?(:id)
      _update(:teams, data)
    else
      _insert(:teams, data)
    end

    data    
  end


  def _get_next_id(key)
    id = 0
    data = @@storage[key] 
    if !data.nil?
      data.each do |item|
        id = item[:id] if item[:id] > id
      end
    end

    id += 1
    id
  end

  def _find_by_id(key, id)
    result = nil

    data = @@storage[key]
    if !data.nil?
      data.each do |item|
        if item[:id] == id
          result = item
          break
        end
      end
    end

    result
  end

  def _insert(key, data)
    if !data.has_key?(:id)
      data[:id] = _get_next_id(key)
    end

    if !@@storage.has_key?(key)
      @@storage[key] = Array.new
    end

    @@storage[key] << data
  end

  def _update(key, data)
    if @@storage.has_key?(key)
      @@storage[key].each do |item|
        if item[:id] == data[:id]
          item = data
          break
        end
      end
    end
  end

  def _destroy(key, id)
    if @@storage.has_key?(key)
      #finish implementation 
    end
  end

end