require 'bcrypt'

module TeamloopServices

  class PasswordService

    def initialize(encrypted_pass)
      @encrypted_pass = encrypted_pass
    end

    def is_equal?(password_str)
      @encrypted_pass == BCrypt::Password.new(password_str)
    end

    def self.create(password_str)
      BCrypt::Password.create(password_str, :cost => 12)
    end

  end


  ### This class should only be used for testing/developmental purposes
  ### Production code should encrypt all passwords.
  class PlainTextPasswordService
    def initialize(encrypted_pass)
      @pass = encrypted_pass
    end

    def is_equal?(password_str)
      @pass == password_str
    end

    def ==(password_str)
      is_equal?(password_str)
    end

    def self.create(password_str)
      password_str
    end
  end

end