module SeasonInteractor

  def self.validate(req)

    #validate 
    #TODO: Add helper function to validate list of keys in hash
    errors = []

    #TODO: add verification that this exists
    season_data = req.data[:season]

    if !season_data.is_a?(Hash)
      errors << { :type => :validation, :message => "Season data is not of the right type" }
    else
      if !season_data.has_key?(:title) 
        errors << { :type => :validation, :field => :title, :message => "Season missing required field: title" }
      else
        if season_data[:title].empty?
          errors << { :type => :validation, :field => :title, :message => "Season must specify valid title" }
        end
      end

      if !season_data.has_key?(:starts_on)
        errors << { :type => :validation, :field => :starts_on, :message => "Season missing required field: Start Date" }
      end

      if !season_data.has_key?(:ends_on)
        errors << { :type => :validation, :field => :ends_on, :message => "Season missing required field: End Date" }
      end

      if season_data.has_key?(:starts_on) && season_data.has_key?(:ends_on)
        if season_data[:ends_on] <= season_data[:starts_on]
          errors << { :type => :validation, :field => :ends_on, :message => "Season must not have an end date earlier than the start date." }
        end
      end

      if errors.size > 0
        res = Response.new(false, { :data => season_data, :errors => errors })
      else
        res = Response.new(true, { :data => season_data })
      end
    end

    res
  end

end