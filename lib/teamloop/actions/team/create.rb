module Teams

  class Create < BaseAction
    required_dependencies :team_gateway
    required_data :team => [:name, :activity, :time_zone]

    def initialize(req, deps = { :team_gateway => TeamloopConfig.EntityGateway })
      super(req, deps)
    end

    def _execute
      # data should have been verified by is_valid action at this point
      data = @dependencies[:team_gateway].save_team(@request.data[:team])
      resp = Response.new(true, { :data => data })

      @response_verified = true

      resp
    end

  end

end