module Teams

  class CanCreate < BaseAction

    def _execute
      #TODO: Need to verify user for specified identity has access to create teams
      resp = Response.new(true) 

      @response_verified = true

      resp
    end

  end

end