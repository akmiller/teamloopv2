module Teams

  class GetAllForUser < BaseAction
    required_dependencies :team_gateway

    def initialize(req, deps = { :team_gateway => TeamloopConfig.EntityGateway })
      super(req, deps)
    end

    def _execute
      data = @dependencies[:team_gateway].find_teams(@request)
      resp = Response.new(true, { :data => { :teams => data }})

      @response_verified = true

      resp
    end

  end

end