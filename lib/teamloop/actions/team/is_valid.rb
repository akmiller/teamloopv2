#require 'teamloop/entity_gateways/team_gateway'

module Teams

  class IsValid < BaseAction
    required_data :team => [:name, :activity, :time_zone]

    def _execute
      # inputs have been verified, here we need to validate that inputs are actually valid

      #TODO: validate that data provided meets our validation rules

      @response_verified = true
      res = Response.new(true, { :data => @request.data })
    end

  end

end