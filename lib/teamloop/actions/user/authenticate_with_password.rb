module Users

  class AuthenticateWithPassword < BaseAction
    required_dependencies [:user_gateway, :password_service]
    required_data :credentials => [:email, :password]

    def initialize(req, deps = { :user_gateway => TeamloopConfig.EntityGateway, :password_service => TeamloopServices::PasswordService })
      super(req, deps)
    end

    def _verify
      @request_verified = true
    end

    def _execute
      credentials = @request.data[:credentials]

      user_info = @dependencies[:user_gateway].find_user_by_email(credentials[:email])

      if user_info.nil?
        resp = Response.new(false, { :data => nil, :errors => [{ :type => :authorization, :message => "Could not locate user by the provided email." }] })
      else
        password = @dependencies[:password_service].new(user_info[:password])
        if password == credentials[:password]
          resp = Response.new(true, { :data => { :user => user_info } })
        else
          resp = Response.new(false, { :data => nil, :errors => [{ :type => :authorization, :message => "Invalid email and/or password." }] })
        end 
      end

      @response_verified = true
      resp
    end

  end

end