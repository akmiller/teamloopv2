module Users 

  class Create < BaseAction
    required_dependencies [:user_gateway, :password_service]
    required_data :user => [:email, :password]

    def initialize(req, deps = { :user_gateway => TeamloopConfig.EntityGateway })
      super(req, deps)
    end

    def _verify
      @request_verified = true
    end

    def _execute
      @response_verified = true

      @request.data[:user][:password] = @dependencies[:password_service].create(@request.data[:user][:password])
      user_data = @dependencies[:user_gateway].save_user(@request.data)

      resp = Response.new(true, { :data => user_data })
    end

  end

end