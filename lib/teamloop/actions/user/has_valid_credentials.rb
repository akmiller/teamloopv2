###
### Makes sure that credentials are provided and match rules for having valid credentials. Having valid credentials does not mean
### the user has been authenticated with the site. That should be ran after this action if attempting to authenticate.
###
module Users


  class HasValidCredentials < BaseAction
    required_data :user => [:email, :password]

    @@MIN_PASSWORD_LENGTH = 8

    def initialize(req, deps = { :team_gateway => TeamloopConfig.EntityGateway })
      super(req, deps)
    end

    def _verify
      @request_verified = true
    end

    def _execute
      #todo verify valid email
      unless @request.data[:user][:email].index("@") > 0
        @errors << { :type => :validation, :message => "Provided email address is not valid" }
      end

      unless @request.data[:user][:password].length >= @@MIN_PASSWORD_LENGTH
        @errors << { :type => :validation, :message => "Provided password must be longer than #{@@MIN_PASSWORD_LENGTH} characters" }
      end

      res = Response.new(@errors.empty?, { :data => @request.data, :errors => @errors })

      @response_verified = true
      res
    end

  end

end