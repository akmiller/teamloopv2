class BaseAction
  attr_accessor :request, :dependencies, :request_verified, :response_verified, :errors

  def initialize(req, deps = {})
    # verify required dependencies
    unless self.class.dependencies.nil?
      self.class.dependencies.each { |dep| raise "Must provide required dependencies: #{self.class.dependencies}." unless deps.include?(dep) }
    end

    @request = req
    @dependencies = deps

    @request_verified = false
    @response_verified = false

    @errors = Array.new
  end

  # Method to be called for running the action. Method verifies authorization flags and runs appropriate methods.
  # This method should either return Response object or raise error.
  def run
    #verify that required data is provided
    verify_required_data
    unless @errors.empty?
      resp = Response.new(false, { :errors => @errors })
    else
      _verify

      if @request_verified
        resp = _execute
        raise "Action must set the response_verified flag to allow response to be recieved." unless @response_verified
      else
        if @errors.empty?
          @errors << { :type => :unkown, :message => "Unable to verify request." }
        end

        resp = Response.new(false, { :errors => @errors })
      end
    end
      
    yield resp
  end

  def _verify
    # Expects no return value. This method should update the request_verified flag appropriately

    # Responsibility of this method is to simply validate that the request is not malformed in any way. Any 
    # security checks or application specific validation should take place in _execute and send back appropriate 
    # response.

    #default implementation
    if @request.identity.nil?
      @request_verified = false
      @errors << { :type => :authorization, :message => "Missing identity" }
    else
      @request_verified = true
    end
  end

  def _execute
    #IMPLEMENTED IN SUBCLASS

    # Expects Response object as return value. This method should update the response_verified flag appropriately
  end

  #TODO: expand this a bit and make it a little better at handling required data more than 1 level deep
  def verify_required_data
    return if self.class.data.nil? || self.class.data.empty?

    self.class.data.keys.each do |key|
      if @request.data.nil? || !@request.data.keys.include?(key)
        @errors << { :type => :validation, :message => "Request data is missing value for key: #{key}" }
      end

      if self.class.data[key].kind_of?(Array)
        self.class.data[key].each do |sym|
          if @request.data.nil? || !@request.data[key].include?(sym)
            @errors << { :type => :validation, :message => "Request data for #{key} missing data for #{sym}" }
          end
        end
      end
    end
  end

  def self.required_dependencies(dependencies)
    unless dependencies.kind_of?(Array)
      deps = Array.new
      if dependencies.kind_of?(Hash)
        deps << dependencies.keys
      else
        deps << dependencies # must be single value
      end
      dependencies = deps
    end

    name = "dependencies"
    ivar_name = "@#{name}"

    instance_variable_set(ivar_name, dependencies)

    self.class.send(:define_method, name) do
      instance_variable_get(ivar_name)
    end
  end

  def self.required_data(data)
    # data may be hash, symbol, or array of symbols
    # convert all to store hash of keys mapped to empty values if no sub-keys or arrays if subkey, won't got more than 2 levels deep for now
    unless data.kind_of?(Hash)
      req_data = Hash.new

      if data.kind_of?(Symbol)
        req_data[data] = ""
      else
        if data.kind_of?(Array)
          data.each do |itm|
            req_data[itm] = ""
          end
        else
          raise "Invalid data provided to required_data of action."
        end
      end

      if req_data.nil?
        req_data = { :test => "test" }
      end 
      data = req_data
    end

    # create ivar
    name = "data"
    ivar_name = "@#{name}"

    instance_variable_set(ivar_name, data)

    self.class.send(:define_method, name) do
      instance_variable_get(ivar_name)
    end
  end

end