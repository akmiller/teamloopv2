=begin
Response will contain valid flag, relevant data and an array of errors

Errors will be an array of hashes that contain a "type" and "message", and if appropriate additional data relevante to error like ":field"
Error "types" will be:

:authorization
:validation
:invalid_request
:not_found
:unknown

=end
class Response
  attr_reader :success, :data, :errors, :authorization_verified

  def initialize(success, options={ :data => nil, :errors => [] })
    @success = success 
    @data = options[:data]
    @errors = options[:errors]
  end

  def success?
    @success
  end

end