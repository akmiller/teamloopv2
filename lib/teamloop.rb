# main teamloop.rb library file

# add current directory to ruby path
$: << File.dirname(__FILE__)
$:.unshift("lib")


require 'teamloop_config'
require 'identity'
require 'request'
require 'response'

# require entities
#require 'teamloop/entities/team'
#require 'teamloop/entities/identity'

# require services
require 'teamloop/services/password_service'

# require actions
require 'teamloop/actions/base_action'

# user actions
require 'teamloop/actions/user/has_valid_credentials'
require 'teamloop/actions/user/authenticate_with_password'
require 'teamloop/actions/user/create'

# team actions 
require 'teamloop/actions/team/can_create'
require 'teamloop/actions/team/is_valid'
require 'teamloop/actions/team/create'
require 'teamloop/actions/team/get_by_id'
require 'teamloop/actions/team/get_all_for_user'

# season actions 
require 'teamloop/actions/season/validate'

# action queues
require 'teamloop/actionqueues/action_queue.rb'

# require gateways
require 'teamloop/gateways/entity_gateway'
require 'teamloop/gateways/inmemory_entity_gateway'


# Setup of all configurable items within teamloop
TeamloopConfig.configure do
  # Persistence operations
  parameter :EntityGateway
  # Background jobs
  parameter :Queue
end


