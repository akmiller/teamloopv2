class Request 
  attr_reader :identity, :data, :options

  def initialize(identity, data = nil, options = {})
    @identity = identity 
    @data = data 
    @options = options
  end

end