=begin
Identity will represent the authenticated information for a given user, api, etc...

DESIGN/INFO still need to be determined, should support carrying identification tokens for common authentication
systems such as OAuth, etc...  

=end
class Identity
  attr :type, :token, :options, :validated

  # type: describes type of authentication (forms, OAuth, etc...)
  # token: token for authentication
  # options: additional data, if from forms may included email, password
  def initialize(type, token, options = {})
    @type = type
    @token = token
    @options = options
    @validated = false
  end

  def validated?
    @validated
  end

  def self.create_test_identity
    identity = Identity.new(:forms, "ABCDEFG", { :email => "akmiller@gmail.com", :password => "teamloopv2" })
    identity._set_validated(true)

    identity    
  end

  # should be used by internal classes only after identity has been validated appropriately
  def _set_validated(validated)
    @validated = validated
  end

end